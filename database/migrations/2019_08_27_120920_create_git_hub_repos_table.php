<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGitHubReposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('git_hub_repo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cache_id')->unsigned();
            $table->string('name');
            $table->string('url');
            $table->string('branches_url');
            $table->string('default_branch');
            $table->dateTime('created_at');
            $table->dateTime('last_commit_date');

        });

        Schema::table('git_hub_repo', function (Blueprint $table) {
            $table->foreign('cache_id')->references('id')->on('git_hub_cache')->onDelete('cascade')->onUpdate('cascade'); // set foreign key
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('git_hub_repo');
    }
}
