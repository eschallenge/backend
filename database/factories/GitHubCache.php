<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\GitHubCache::class, function (Faker $faker) {
    return [
        'cached_at' => $faker->dateTime
    ];
});
