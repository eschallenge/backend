<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\GitHubRepo::class, function (Faker $faker) {
    return [
        //'cache_id'
        'name' => 'name',
        'url' => 'https://github.com/githubtest',
        'branches_url' => 'https://github.com/githubtest',
        'default_branch' => 'master',
        'created_at' => '2019-08-09 13:02:18',
        'last_commit_date' => '2019-08-09 13:02:18',
    ];
});
