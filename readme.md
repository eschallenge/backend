You can find detailed Laravel documentation at https://laravel.com/docs/5.8

# Requirements

PHP v7.1.3 or higher

# Setup

run composer install to install all dependencies 

point your web server's document/web root to be the /public directory 

# Database 
run php artisan migrate to build tables (must have database credentials in .env file)

Rename .env.example to .env and change the following database credentials to yours. These can also be changed in 
config/database.php

DB_CONNECTION=mysql
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

# API 

All routes are defined in the routes/api.php file. By default Laravel adds a prefix of 'api/' so to access /repos you 
would need to call /api/repos

# app\Http\Controllers\GitHubReposController 

This controller contains the logic for storing the top 10 most recent repos in the database. 

# Testing

The coverage reports are in the coverage-report folder if you would like to view them.

The unit tests are located in tests/Unit/GitHubRepoTest.php

'phpunit --coverage-html ./coverage' runs the tests and generates the coverage reports in /coverage.

NOTE: I did not create Unit Tests for Laravels built-in functionality like authentication. I have highlighted 
all of the Unit Tests I created in the screen shot below. All of the code I wrote has tests written for it.

![Coverage](coverage.png)
![Coverage_2](coverage_1.png)
![Coverage_3](coverage_2.png)











