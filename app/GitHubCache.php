<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GitHubCache extends Model
{
    protected $table = 'git_hub_cache';
    public $timestamps = false;

    protected $fillable = [
        'cached_at'
    ];

    public function repos() {
        return $this->hasMany(GitHubRepo::class, 'cache_id');
    }

}
