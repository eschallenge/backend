<?php

namespace App\Http\Controllers;

use App\GitHubCache;
use App\GitHubRepo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GitHubReposController extends Controller {

    /**
     * Get the top 10 most recent repos and store them
     * @param Request $request
     */
    public function cacheResults(Request $request) {

        $model = new GitHubRepo();

        $data = $request->all();
        $now = Carbon::now();
        $validator = Validator::make($data, $model->getValidationRules()); // create the validator

        // Validate the data, if it fails pass back the first error message to the user
        if($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 409);
        }

        try
        {
            $cache = new GitHubCache();
            $cache->cached_at = $now;

            $cache->save();

            foreach($data as $repo)
            {
                $model = new GitHubRepo();
                $model->cache_id = $cache->id;
                $model->name = $repo["name"];
                $model->url = $repo["url"];
                $model->branches_url = $repo["branches_url"];
                $model->default_branch = $repo["default_branch"];
                $model->created_at = Carbon::parse($repo["created_at"]);
                $model->last_commit_date = Carbon::parse($repo["last_commit_date"]);

                $model->save();
           }
        }
        catch(\Exception $ex) {
            return response()->json(['error' => 'An exception has occurred'], 400);
        }
        return response()->json(['message' => 'Data has been cached'], 200);
    }

    public function getCachedData() {
        return GitHubCache::with('repos')->orderByDesc('cached_at')->limit(1)->get();
    }
}
