<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GitHubRepo extends Model
{
    protected $table = 'git_hub_repo';
    public $timestamps = false;

    // Columns that are required
    protected $fillable = [
       // 'cache_id',
        'name',
        'url',
        'branches_url',
        'default_branch',
        'created_at',
        'last_commit_date'
    ];

    // Return the validation rules
    public function getValidationRules() {
        return [
            '*.name' => 'required|string|max:255',
            '*.url' => 'required|string|max:255',
            '*.branches_url' => 'required|url|max:255',
            '*.default_branch' => 'required|string|max:255',
            '*.created_at' => 'required|date|max:255',
            '*.last_commit_date' => 'required|date|max:255',
        ];
    }

}
