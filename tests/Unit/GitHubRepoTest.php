<?php

namespace Tests\Unit;

use App\GitHubCache;
use App\GitHubRepo;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GitHubRepoTest extends TestCase
{
    /**
     * Tests getting data from the server.
     */
    public function testGetCacheResponseValid() {
        $response = $this->json('GET', 'api/repos');
        $response->assertStatus(200);
    }

    /**
     * Tests JSON structure of data returned from the server.
     */
    public function testGetCacheResponseStructure() {
        $response = $this->json('GET', 'api/repos');
        $response->assertJsonStructure([[
            'id',
            'cached_at',
            'repos' => [
                '*' => [
                    'cache_id',
                    'name',
                    'url',
                    'branches_url',
                    'default_branch',
                    'created_at',
                    'last_commit_date'
                ]
            ]]
        ]);
    }

    /**
     * Tests saving data to the server. A successful response with return with a
     * status of 200 & the message "Data has been cached"
     */
    public function testCreateCachedData() {
        $cache = factory(GitHubCache::class)->create();
        $repo = factory(GitHubRepo::class, 3)->create(['cache_id' => $cache->id]);
        $response = $this->post('/api/repos/cache', $repo->toArray());
        $response->assertStatus(200);
        $response->assertSeeText("Data has been cached");
    }

    /**
     * Tests validating data. Should return a 409 response because
     * the structure of the data is incorrect
     */
    public function testValidation() {
        $this->withoutExceptionHandling();
        $data = [
            'cache_id' => 3,
            'id' => 4,
            'name' => 'testing git',
            'url' => 'https://github.com/githubtest',
            'branches_url' => 'https://github.com/githubtest/repos/master',
            'default_branch' => 'master',
            'created_at' => '2019-08-09 13:02:18',
            'last_commit_date' => '2019-08-09 13:02:18'
        ];

        $response = $this->post('/api/repos/cache', $data, ['Accept' => 'application/json']);
        $response->assertStatus(409);
    }
}
